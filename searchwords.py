#!/usr/bin/env pythoh3

'''
Busca una palabra en una lista de palabras
'''

import sortwords
import sys

def search_word(word, words_list):
    found = False
    for i in  words_list:
        if sortwords.equal(word, i):
            test_index = words_list
            found = True
            return test_index
    if not found:
        raise Exception


"""Lee parámetros en línea de comandos (palabra, lista). 
Primero se llama a la función sort (del fichero sortwords.py) que
ordena, después llama a search_word para obtener la posición."""
def main():
    word = sys.argv[1]
    words_list = sys.argv[2]

    ordered_list = sortwords.sort(words_list)
    position = search_word(word, ordered_list)
    sortwords.show(ordered_list)
    print(position)


if __name__ == '__main__':
    main()
